# Dockerstead Minimal

Dockerstead Minimal is a lightweight environment for Laravel and Lumen development. Unlike Homestead, this image is designed for API development thus only a handful PHP extensions are installed. The environment is composed of Nginx, PHP7, Redis, and PHP-FPM on Alpine Linux. For ultra-lightweight images, the [PHP official repository](https://hub.docker.com/_/php/) has several PHP- and PHPFPM-only images.

## Usage

For local development:
```bash
docker run -d -p 80:80 -v [path-to-laravel-project]:/var/www/app drewfle/laravel-dockerstead-minimal
```

For simulating production environment or deploying on server:
```
FROM drewfle/laravel-dockerstead-minimal:latest

WORKDIR /var/www
COPY app/ app/
RUN chown -R :www-data app && \
    chmod -R 750 app && \
    chmod g+s app && \
    chmod -R app app/storage && \
    chmod -R 775 app/bootstrap/cache
```

Launch an interactive terminal within Dockerstead Minimal:

```
docker exec -it [container-name/container-id] ash
```

Add additional packages:
```
FROM drewfle/laravel-dockerstead-minimal:latest

RUN apk --update\
    --repository=http://nl.alpinelinux.org/alpine/edge/testing add \
    php7-mongodb \
    [more-packages...]

RUN apk --update add \
    git \
    [more-packages...]

# `--no-cache` tag doesn't work as intended, do it manually:
RUN rm /var/cache/apk/*
```

To modify Nginx or PHP-FPM conf file, just overwrite the existing `/etc/nginx/nginx.conf` or `/etc/php7/php7-fpm.d/www.conf` with `ADD` or `COPY` command or edit in place with `sed` in the Dockerfile.

## Installed Packages

Daemons:
- `nginx`
- `redis`
- `php7-fpm`
- `supervisor`

Laravel minimal requirements (since Laravel 5.1, `mcrypt` is no longer needed):
- `php7`
- `php7-openssl`
- `php7-pdo_mysql`
- `php7-mbstring`
- `php7-session`

Additional packages:
- `php7-pdo_sqlite`
- `php7-json`
- `php7-xml`
- `php7-dom`
- `php7-curl`
- `php7-phar`
- `php7-zip`
- `php7-redis`
- `php7-opcache`
- `curl`

## Ports

```bash
/ # netstat -nlpt
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address  Foreign Address State  PID/Program name
tcp        0      0 0.0.0.0:6379   0.0.0.0:*       LISTEN 10/redis-server
tcp        0      0 0.0.0.0:80     0.0.0.0:*       LISTEN 8/nginx
tcp        0      0 127.0.0.1:9000 0.0.0.0:*       LISTEN 9/php-fpm.conf)
tcp        0      0 :::6379        :::*            LISTEN 10/redis-server
tcp        0      0 :::80          :::*            LISTEN 8/nginx
```
